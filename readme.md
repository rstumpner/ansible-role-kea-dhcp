# Ansible Role for ISC Kea DHCP Server

This is a Ansible Role ISC Kea DHCP Server Installation and Configuration. The Role is tested against a CI/CD Pipeline in Gitlab and have some Tags on the Tasks to Impelement some capabilities for a devops style deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-keadhcp
```

## Stages
#### Stage Build the keadhcp Configuration 
Build the keadhcp Configuration in to copy paste it to a installed kea dhcp server

- tag: build

###### Steage prepare keadhcp Install
Install the Packages on Ubuntu

- tag: prepare

Tested:
 - Vagrant Ubuntu 18.04
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at